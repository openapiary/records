import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';
import * as uuid from 'uuid';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import proxy from './middleware/proxy';
import AppModule from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = app.get(ConfigService);
  const logger = await app.resolve(PinoLogger);

  app.use((req, res, next) => {
    req.id = req.headers[config.get('service.correlationId')] ?? uuid.v4();

    logger.debug({ req }, 'incoming request');
    next();
  });

  ['/api/v1/organization', '/api/v1/role', '/api/v1/user'].map((route) =>
    app.use(
      route,
      proxy(config.get('server.proxy.auth'), logger, config, {
        pathRewrite: {
          '^/api/v1': '',
        },
      }),
    ),
  );

  if (config.get<boolean>('swagger.enabled', false)) {
    logger.debug('Swagger enabled');
    const document = SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .setTitle(process.env.npm_package_name)
        .setDescription(process.env.npm_package_description)
        .setVersion(process.env.npm_package_version)
        .setContact(
          process.env.npm_package_author_name,
          process.env.npm_package_author_url,
          process.env.npm_package_author_email,
        )
        .setLicense(
          process.env.npm_package_license,
          `https://opensource.org/licenses/${process.env.npm_package_license}`,
        )
        .addBearerAuth(
          {
            type: 'http',
            scheme: 'bearer',
            description: 'Authentication token',
            bearerFormat: 'JWT',
          },
          'jwt',
        )
        .build(),
    );
    SwaggerModule.setup(config.get('swagger.path'), app, document);
  } else {
    logger.debug('Swagger disabled');
  }

  if (config.get<boolean>('server.enableShutdownHooks')) {
    logger.debug('Enabling shutdown hooks');
    app.enableShutdownHooks();
  } else {
    logger.debug('Shutdown hooks not enabled');
  }

  logger.debug('Starting HTTP listener');
  const port = config.get('server.port');
  await app.listen(port);
  logger.info({ port }, 'Application running');
}

bootstrap().catch((err) => {
  /* Unlikely to get to here but a final catchall */
  console.log(err.stack);
  process.exit(1);
});
