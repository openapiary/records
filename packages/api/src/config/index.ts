import logger from './logger';
import server from './server';
import swagger from './swagger';

export default [logger, server, swagger];
