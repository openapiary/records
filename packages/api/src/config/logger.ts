import { registerAs } from '@nestjs/config';
import { Params } from 'nestjs-pino';
import * as pino from 'pino';

export default registerAs(
  'logger',
  (): Params => ({
    pinoHttp: {
      level: process.env.LOG_LEVEL ?? 'trace',
      serializers: pino.stdSerializers,
      genReqId: (req) => req.id,
      customLogLevel: (res, err) => {
        const { statusCode } = res;
        if (statusCode > 400 && statusCode !== 404 && statusCode < 500) {
          return 'warn';
        }
        if (statusCode >= 500 || err) {
          return 'error';
        }

        return 'debug';
      },
    },
  }),
);
