import { registerAs } from '@nestjs/config';

export default registerAs('server', () => ({
  correlationId: process.env.LOG_CORRELATION_ID ?? 'x-correlation-id',
  enableShutdownHooks: process.env.SERVER_ENABLE_SHUTDOWN_HOOKS !== 'false',
  port: Number(process.env.SERVER_PORT ?? 3000),
  proxy: {
    auth: process.env.AUTH_SERVICE_URL,
  },
}));
