import { createProxyMiddleware } from 'http-proxy-middleware';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';

function logFactory(logger, level) {
  return (...args) => logger[level](...args);
}

export default (
  target: string,
  logger: PinoLogger,
  config: ConfigService,
  { pathRewrite = undefined }: { pathRewrite?: any } = {},
): any =>
  createProxyMiddleware({
    target,
    changeOrigin: true,
    pathRewrite,
    logLevel: 'debug', // Let main logger handle this
    logProvider: () => ({
      log: logFactory(logger, 'info'),
      debug: logFactory(logger, 'debug'),
      info: logFactory(logger, 'info'),
      warn: logFactory(logger, 'warn'),
      error: logFactory(logger, 'error'),
    }),
    onProxyReq(proxyReq, req) {
      proxyReq.setHeader(config.get('server.correlationId'), req.id as string);
    },
    onProxyRes(proxyRes, req) {
      logger.debug({ req, res: proxyRes }, 'request completed');
    },
  });
