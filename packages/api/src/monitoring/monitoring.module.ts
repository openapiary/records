import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { PrometheusModule } from '@willsoto/nestjs-prometheus';
import HealthController from './health.controller';
import MetricsController from './metrics.controller';

@Module({
  imports: [
    TerminusModule,
    PrometheusModule.register({ controller: MetricsController }),
  ],
  controllers: [HealthController],
})
export default class MonitoringModule {}
