import { Controller, Get, Inject } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckService,
  HttpHealthIndicator,
} from '@nestjs/terminus';
import { ConfigService } from '@nestjs/config';
import { ApiOperation } from '@nestjs/swagger';

@Controller('/health')
export default class HealthController {
  @Inject(ConfigService)
  private readonly config: ConfigService;

  @Inject(HealthCheckService)
  private readonly health: HealthCheckService;

  @Inject(HttpHealthIndicator)
  private readonly http: HttpHealthIndicator;

  @Get()
  @HealthCheck()
  @ApiOperation({ tags: ['monitoring'], summary: 'Perform API health checks' })
  healthCheck() {
    return this.health.check([
      () =>
        this.http.pingCheck(
          'auth-service',
          `${this.config.get('server.proxy.auth')}/health`,
        ),
    ]);
  }
}
