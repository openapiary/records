import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule, Params } from 'nestjs-pino';
import MonitoringModule from './monitoring/monitoring.module';
import config from './config';

@Module({
  imports: [
    ConfigModule.forRoot({
      ignoreEnvFile: true,
      isGlobal: true,
      load: config,
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<Params> =>
        configService.get<Params>('logger'),
    }),
    MonitoringModule,
  ],
  controllers: [],
  providers: [],
})
export default class AppModule {}
